/**
 *	Wrapper used by an indexer to manage various encodings and calculation results of Match Algorithms
 *	To-Do: Implement logic to do and store match results.
 */
public class MatchResult {

    public static final String ALGO_SOUNDEX = 'SOUNDEX';
    public static final String ALGO_METAPHONE = 'METAPHONE';
    public static final String ALGO_DOUBLEMETAPHONE = 'DOUBLEMETAPHONE';
    public static final String ALGO_STRINGCOMPARE = 'STRING_COMPARATOR';
    public static final String ALGO_LEVENSHTEIN = 'LEVENSHTEIN';
    private static final Integer LEVENSHTEIN_DIST = 5;
    private static final String  SIMILARITY_DIST = '.6f';

    private String key = null;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Returns the name of the algorithm that was used to generate this match result.
     */    
    public String getAlgorithm() {
        return null;
    }
    
    public void setAlgorithm() {
        // Describes which algorithm to use
    }
}