/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class SoundexTest {

	// Assert that when two strings are not the same, they receive different encodings
    static testMethod void testNotMatchesEncoding() {
		Soundex soundex = new Soundex();
		String input;
		String compare;
		input = 'United States of America';
		compare = 'Ukraine';
		System.assertNotEquals(soundex.compute(input), soundex.compute(compare));
    }
    
    // Assert that when two strings are exactly the same, they receive the same exact encoding
    static testMethod void testMatchesEncoding() {
		Soundex soundex = new Soundex();
		String input;
		String compare;
		input = 'United States of America';
		compare = 'United States of America';
		System.assertEquals(soundex.compute(input), soundex.compute(compare));
    }
    
    // Assert that when two strings are phonetically similar, they receive the same exact encoding
    static testMethod void testSimilarMatchesEncoding() {
		Soundex soundex = new Soundex();
		String input;
		String compare;
		input = 'Swedish House Mafia';
		compare = 'Sweedish House Mafeia';
		System.assertEquals(soundex.compute(input), soundex.compute(compare));
    }
    
    // Test null input
    static testMethod void testNull() {
    	String input = null;
    	Soundex soundex = new Soundex();
    	System.assertEquals(null, soundex.compute(input));
    }
    
    // Test for padding in result
    static testMethod void testPadding() {
    	String input = 'dog';
    	Soundex soundex = new Soundex();
    	System.assertEquals('0', soundex.compute(input).substring(3, 4));
    }
    
    // Assert that the length modifiers work correctly
    static testMethod void testOutputLength() {
    	Soundex soundex = new Soundex();
    	Integer newLength = 6;
    	String input = 'Hack the planet';
    	System.assertEquals(4, soundex.getLength());
    	soundex.setLength(newLength);
    	System.assertEquals(6, soundex.getLength());
    	System.assertEquals(6, soundex.compute(input).length());
    }
}