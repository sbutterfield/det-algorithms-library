/**
 *  Provides utility methods necessary for matcher services.
 *  @author = Shawn Butterfield
 *  @date = 11/14/2013
 */
global class MatcherUtils {

    //private static final String MARKERS_REGEX = getMarkers();

    private static final Set<String> ILLEGAL_MARKERS = new Set<String> {
        '.'
    };
	
    private MatcherUtils() {
		// Not a valid object for construction
	}

    public static String getCleanString(String input) {
        String result = input;

        if (input == null) {
            return result;
        }

        result = result.replaceAll('[^a-zA-Z ]', '');
        result = removeWhitespace(result);

        return result;
    }

    public static String removeWhitespace(String input) {
        String result;
        if (input == null) {
            return result;
        }
        return input.replaceAll('(\\s+)', '');
    }

    private static String getMarkers() {
        String result = '';
        String resourceName = 'REGEX_MARKERS';

        StaticResource resource = [SELECT Name,NamespacePrefix,Body FROM StaticResource WHERE Name = :resourceName LIMIT 1];

        if (resource != null) {
            result = String.valueOf(resource.Body);
        }

        return result;
    }
}