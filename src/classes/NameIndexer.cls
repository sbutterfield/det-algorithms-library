public class NameIndexer { 

	private static Map<String,List<String>> wordIndex = new Map<String,List<String>>();

	private static Algorithm[] algo = new Algorithm[0];
	
	static {
		algo.add(new DoubleMetaphone());
	}

	private NameIndexer() {}

	static void indexName(String name) {
		if (name == null) {
			return;
		}
		name = name.toUpperCase(); 

		for (Algorithm a : algo) {
			Algorithm algorithm = a;
			algorithm.processPhrase(name);
		}
	}

	static void printIndex() {
		System.debug(wordIndex);
	}

	static void addWordIndex(String index, String word) {
		List<String> words = wordIndex.get(index);

		if (words != null) {
			words.add(word);
		} else {
			List<String> wordList = new List<String>();
			wordList.add(word);
			wordIndex.put(index, wordList);
		}
	}

	static List<String> getSoundExCodeMatches(String soundExCode) {
		return wordIndex.get(soundExCode);
	}

	public static List<PhraseMatchResults> findMatchingPhrases(String word) {
		if (word == null) {
			return new List<PhraseMatchResults>();
		}
		word = word.toUpperCase();

		List<PhraseMatchResults> results = new List<PhraseMatchResults>();
		for (Algorithm a : algo) {
			Algorithm algorithm = a;
			results.add(algorithm.findMatchingPhrases(word));
		}
		return results;
	}

}