public interface MatchAlgorithm {

	/**
	 * To-do: Implement contract for all algorithms. What should all algorithms know how to do?
	 * Phonetics should know how to encode
	 * Distance and sorters should know how to compute, maybe this is universal for all and should drop the encode
	 * Should know how to compare one input to another and score? Or provide a match result?
	 * Perhaps there should be a layer of abstraction between the algorithms and their implementation, like a mediator pattern to control how to invoke matching?
	 */
}