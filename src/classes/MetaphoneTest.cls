/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 *  @author = Shawn Butterfield
 *  @date = 11/11/2013
 *  @desc = Tests the Metaphone class using several phonetic encoding patterns.
 */

@isTest
private class MetaphoneTest {
	
	@isTest static void testBasicMetaphone() {
		String s1 = 'Onomatopoeia';
        String expected = 'ONMTP';
        System.assertEquals(Metaphone.getMetaphone(s1), expected);
	}

    @isTest static void testMetaphoneEquals() {
        String s1 = 'Smith';
        String s2 = 'Smyth';
        // s1 == s2
        System.assert(Metaphone.getMetaphoneEquals(s1, s2));

        s2 = 'Smithereens';
        // s1 != s2
        System.assert(!Metaphone.getMetaphoneEquals(s1, s2));

        s2 = null;
        // null param should always return false and not an exception
        System.assert(!Metaphone.getMetaphoneEquals(s1, s2));
    }
	
	@isTest static void testHomonyms() {
		String s1 = 'McAllister';
        String s2 = 'McAllistor';

        System.assert(Metaphone.getMetaphoneEquals(s1, s2));
	}

    @isTest static void testCHX() {
        String s1 = 'Charles Xavier';
        String expected = 'XRLSKSFR';
        System.assertEquals(Metaphone.getMetaphone(s1), expected);
    }

    @isTest static void testGH() {
        String s1 = 'Tough';
        String s2 = 'Thorough';
        String s3 = 'Behest';
        String s4 = 'Hang';

        String expected = 'T';
        System.assertEquals(Metaphone.getMetaphone(s1), expected);

        expected = '0R';
        System.assertEquals(Metaphone.getMetaphone(s2), expected);

        expected = 'BHST';
        System.assertEquals(Metaphone.getMetaphone(s3), expected);

        expected = 'HNK';
        System.assertEquals(Metaphone.getMetaphone(s4), expected);
    }

    @isTest static void testIOU() {
        String s1 = 'Suspicious';
        String s2 = 'Anxious';

        String expected = 'SSPSS';
        System.assertEquals(Metaphone.getMetaphone(s1), expected);

        expected = 'ANKSS';
        System.assertEquals(Metaphone.getMetaphone(s2), expected);
    }

    /**
     *  @TO-DO: Add more unit tests
     */
	
}