/**
 * Custom algorithm to do lexigraphical string order comparison with character-shift calculations.
 * The algorithm will first sort and store the strings, then after finding the middle of the initial string length that should mark the
 * highest comparitivity index point in both string, start searching one character at a time for similarities.
 * For those characters that fall outside of the first strings index, a trimmed score is allotted.
 * Final Calculation:
 *	> For letter positioning that matched in string 1 (shortest string), allocate a .044 weighting to each position index that matched
 *	> For letter positioning that matched in string 2 (longest string), allocate a .92 weighting to each position index that matched
 *	> For letter positions that didn't match, were extra etc. that were added to the trimmed value +.5, subtract from the common score and divide by the common score to ascertain the difference
 *		between the commonly scored characters that did line up, and those characters that didn't and fell outside of the index. If the strings were more alike than they were not alike,
 *		then this should be far less than 1...
 *
 * Result: the method will return the whole integer value comparativity score between the two strings (1-100 scale)
 *
 * @author: Shawn Butterfield, Salesforce.com Inc.
 * @date: March-2013
 */

global class StringComparator {

    /**
    * method to compare the strings and weight them
    * @param string to be compared
    * @param string to be compared
    * @return Integer - weight
    */

    global static Integer compare(String string1, String string2, Integer nullScore) {
        System.debug('@@ Comparing: ' +string1+ ' to this: ' +string2);
        //Return nullScore if one of the values is null
        if (String.isBlank(string1) || String.isBlank(string2)) {
            return nullScore;
        }

        // Otherwise start calculations
        String str1 = MatcherUtils.getCleanString(string1);
        String str2 = MatcherUtils.getCleanString(string2);
        if (str1.equalsIgnoreCase(str2)) {
            return 100;
        }
        else {
            Integer len_str1 = str1.length();
            Integer len_str2 = str2.length();
            
            String a1 = ''; // Becomes substring of str1
            String a2 = ''; // Becomes substring of str2
            Integer f = 0; // f indicates where the inner loop should start from.
            Integer l = 0; // l tells the inner loop how many chars to iterate
            Double tr = 0;
            Double common = 0; // Counter of common chars for each outer loop iteration
            Double comp_value = 0;
            // Tracks character similarities in str1 & str2 at their respective positions
            List<Boolean> f1 = new List<Boolean>();
            List<Boolean> f2 = new List<Boolean>();
        
            //Make sure that the second string is larger than the first. 
            //If not then swap the strings and their length variables
            if (len_str1 > len_str2) {
                Integer swap_len = len_str2;
                len_str2 = len_str1;
                len_str1 = swap_len;
                String swap_str = str1;
                str1 = str2;
                str2 = swap_str;
            }
            
            Integer max_len = len_str2;
            
            //Assign false for all the values assuming that the characters in that position are unequal
            for (Integer i=0; i<=len_str1; i++)
                f1.add(false);
            
            for (Integer j=0; j<=len_str2; j++)
                f2.add(false);
            
            //Allot the median
            // Subtract 1 bc index starts at 0 but length starts at 1
            Integer m = Math.round((max_len / 2) - 1);
            
            /**
             *  Iterate through all the characters in the first string
             *  First outer loop counts how many characters in the first string exist in the second string,
             *  and tracks the index of that occurence.
             *  For each similarity, set index from second string to true within f2
             *  For each similarity, index within f1 remains the same
             */
            for (Integer i=1; i<=len_str1; i++) {
                a1 = str1.substring(i-1, i); // Grab the next character, starting from 0, ending at len_str1
                if (m >= i) {
                    // As long as the median is still bigger than our index, keep going.
                    f = 1;
                    l = i + m;
                }
                else {
                    // Otherwise, see if it is safe to continue.
                    f = i - m;
                    l = i + m;
                }
                
                if (l > max_len) {
                    // We should never go out of bounds of max length
                    l = max_len;
                }
                    
                Boolean flag = false;
                
                //Iterate through all the characters in the second string to see if the characters in that position matches
                for (Integer j=f; j<=l; j++) {
                    a2 = str2.substring(j-1, j); // Grab the next char from str2 (long string)
                    if ((a2.equalsIgnoreCase(a1)) && (f2[j] == false)) {
                        if (!flag) { // For the first pass, if we've found first repeating common, add to common but only once
                            common = common + 1;
                            flag = true;
                        }
                        f1[i-1] = true;
                        f2[j-1] = true;
                        continue;
                    }
                }// End of inner loop 
            
            }// End of outer loop

            /**
             *  Second loop compares similarities between the two strings based on the position of each
             *  similarity and dissimilarity found in first loop.
             *  For each position that reported a true position that was actually not the same char, add
             *  .5 to a penalty score that will hit the computed value later.
             */
            l = 1;
            for (Integer i=1; i<=len_str1; i++) {
               if (f1[i]) {
                    for (Integer j=l; j<=len_str2; j++) {
                        if (f2[j]) {
                            l = j + 1;
                            a1 = str1.substring(i-1, i);
                            a2 = str2.substring(j-1, j);
                            if (!a1.equalsIgnoreCase(a2)) {
                                tr = tr + 0.5;
                            }
                            break;
                        }
                    }// End of inner loop
                }
            } // End of outer loop
            
            Double wcd = 0.044; // string1 common weight
            Double wrd = 0.92;  // string 2 common weight
            Double wtr = 0.041; // false-positives weight
            
            //Calculating the comp_value            
            if (common != 0) {
                comp_value = ((wcd * common) / len_str1) + ((wrd * common) / len_str2) + ((wtr * (common - tr)) / common);
            }
            // Added logical qualifier to ensure both strings are longer than a single character.
            if (len_str1 > 1 && len_str2 > 1) {
                if (str1.substring(0,1) != str2.substring(0,1)) {
                    comp_value = comp_value / 1.274;
                }
            }
            System.debug('Compare Value $$$$$$$$$$$ '+ comp_value + ': String 1 - ' + string1 + '    String 2 - ' + string2 + ' Weight - ' + Math.round(comp_value*100));
            return Math.round(comp_value * 100);
        }
    }
    
    global static Boolean equals(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equals(str2);
    }

    global static Boolean equalsIgnoreCase(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equalsIgnoreCase(str2);
    }
    
    global static boolean isBlank(String str) {
        return str == null || str.trim() == null || str.trim().length() == 0;
    }
}