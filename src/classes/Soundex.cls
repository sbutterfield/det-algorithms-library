/**
 *	Standard Apex Soundex implementation.
 *	CITE: http://arungaikwad.wordpress.com/
 *	March-2013
 */
global class Soundex implements MatchAlgorithm {

	// The higher the threshold for fuzzy matching logic, the higher this length should be.
	private Integer soundexLength = 4;
	
	global void setLength(Integer i) {
		this.soundexLength = i;
	}
	
	global Integer getLength() {
		return this.soundexLength;
	}

	private map<String,String> soundexMap = new map<String,String>{
		'A'=>'0','B'=>'1','C'=>'2','D'=>'3','E'=>'0',
		'F'=>'1','G'=>'2','H'=>'0','I'=>'0','J'=>'2',
		'K'=>'2','L'=>'4','M'=>'5','N'=>'5','O'=>'0',
		'P'=>'1','Q'=>'2','R'=>'6','S'=>'2','T'=>'3',
		'U'=>'0','V'=>'1','W'=>'0','X'=>'2','Y'=>'0','Z'=>'2'
	};
	
	/**
	 * To-Do: Implement a function to return a match result from two inputs.
	 *
	public MatchResult getMatchResult(String input1, String input2) {
		return new MatchResult();
	}
	*/
	
	global String compute(String input){
		String prevChar = ' ';
		
		if (input == NULL || input.length() == 0){
			return null;
		}
		
		String normStr = input.toUpperCase();
		//Append first character to encoded string
		String soundexStr = normStr.substring(0,1);
		integer strLength = normStr.length();
		
		try {
			for (integer i=1; i<strLength && soundexStr.length()<soundexLength; i++){
				String key=normStr.substring(i,i+1);
				String soundexChar = soundexMap.get(key);
		
				if (soundexChar != NULL && !soundexChar.equals('0') && !soundexChar.equals(prevChar)){
					soundexStr = soundexStr+soundexChar;
					prevChar = soundexChar;
				}
			}
		}
		catch (ListException le) {
			throw new ddcLib1.IndexOutOfBoundsException('The index that the algorithm attempted to reference was out of bounds for the input string length ' +le);
		}
		//Pad soundex string if the length is less than 4
		while (soundexStr.length() < soundexLength){
			soundexStr = soundexStr+'0';
		}
		System.debug(LoggingLevel.INFO,'Soundex compute result:  ' +soundexStr);
		return soundexStr;
	}
	
}