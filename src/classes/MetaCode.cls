/**
 * Container class to hold a metacode root value and it's encoding after metaphone algorithm has processed it.
 * This container should be used only by Metaphone and DoubleMetaphone algorithms
 */
public class MetaCode {

	public String root { get; set; }
	public String encoding { get; set; }
	
	/**
	 * Default constructor is hidden, MetaCode must be constructed from a root and an encoding
	 */
	public MetaCode(String root, String encoding) {
		if(root != null) {
			this.root = root;
		}
		else {
			throw new IllegalArgumentException('Cannot construct an instance of MetaCode without a root string');
		}
		this.encoding = encoding;
	}
	
	private MetaCode(){}
}