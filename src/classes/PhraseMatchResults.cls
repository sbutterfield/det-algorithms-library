public class PhraseMatchResults {

	public static final String ALGO_SOUNDEX = 'SOUNDEX';
	public static final String ALGO_METAPHONE = 'METAPHONE';
	public static final String ALGO_DOUBLEMETAPHONE = 'DOUBLEMETAPHONE';
	private static final Integer LEVENSHTEIN_DIST = 5;
	private static final String  SIMILARITY_DIST = '.6f';

	private Set<String> exactMatches = new Set<String>();
	private Set<String> nearMatches = new Set<String>();
	private String key = null;

	private String algoName = null;

	public String getAlgoName() {
		return algoName;
	}

	public void setAlgoName(String algoName) {
		this.algoName = algoName;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void addExactMatch(String word) {
		this.exactMatches.add(word);
	}

	public void addNearMatch(String word) {
		if (withinLDRange(word)) {
			this.nearMatches.add(word);
		}
	}

	private boolean withinLDRange(String word) {
		Integer dist = word.getLevenshteinDistance(this.key);
		if (dist > LEVENSHTEIN_DIST) {
			return false;
		}
		return true;
	}

	public void addNearMatch(List<String> words) {
		this.nearMatches.addAll(words);
	}

	public void addExactMatch(List<String> words) {
		this.exactMatches.addAll(words);
	}

	public List<String> getExactMatches() {
		return sortExactSet();
	}

	public List<String> getNearMatches() {
		return sortNearSet();
	}

	private List<String> sortExactSet() {
		List<String> exactMatchList = new List<String>();
		exactMatchList.addAll(exactMatches);

		SortComparator comp = new SortComparator();
		comp.setKey(this.key);
		exactMatchList.sort();

		return exactMatchList;
	}

	private List<String> sortNearSet() {
		List<String> nearMatchList = new List<String>();
		nearMatchList.addAll(nearMatches);
		LevenshteinDistanceComparator comp = new LevenshteinDistanceComparator();
		comp.setKey(this.key);
		nearMatchList.sort();

		return nearMatchList;
	}

	class SortComparator {

		private String key = null;

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public Integer compare(String o1, String o2) {

			Integer index1 = o1.indexOf(key);
			Integer index2 = o2.indexOf(key);

			if(index1 >=0 && index2 >=0) {

				if (index1 == index2) {
					return 0;
				}

				if (index1 > index2) {
					return 1;
				}

				if (index1 < index2) {
					return -1;
				}
			}

			if (index1 >=0 && index2 < 0) {
				return -1;
			}

			if (index1 <0 && index2 >= 0) {
				return 1;
			}

			return -1;
		}

	}

	class LevenshteinDistanceComparator {

		private String key = null;

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public Integer compare(String o1, String o2) {

			Integer dist1 = key.getLevenshteinDistance(o1);
			Integer dist2 = key.getLevenshteinDistance(o2);

			if(dist1 >=0 && dist2 >=0) {

				if (dist1 == dist2) {
					return 0;
				}

				if (dist1 > dist2) {
					return 1;
				}

				if (dist1 < dist2) {
					return -1;
				}
			}
			return 1;
		}

	}

}